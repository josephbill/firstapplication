package com.emobilis.firstandroidclass;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class ProductDetails extends AppCompatActivity {
    //declare elements
    ImageView imageProduct;
    TextView tvName, tvDesc, tvPrice, tvPhone;
    //variables to hold shared information
    String name, description, price, phone, image;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details);

        //reference
        imageProduct = findViewById(R.id.imageView2);
        tvDesc = findViewById(R.id.textView4);
        tvName = findViewById(R.id.textView3);
        tvPrice = findViewById(R.id.textView5);
        tvPhone = findViewById(R.id.textView6);

        //using intents to fetch shared data from previous class
        Intent intent = getIntent();  //intializing
        name = intent.getStringExtra("productName");
        description = intent.getStringExtra("productDesc");
        price = intent.getStringExtra("productPrice");
        phone = intent.getStringExtra("contactPhone");
        image = intent.getStringExtra("productImage");

        //to fetch image
        Bundle bundle = getIntent().getExtras();
        //i have check if there is an image being shared
        if (bundle != null) {
            //create a variable to store my image
            int resid = bundle.getInt("productImage");
            imageProduct.setImageResource(resid); //set image to the imageview
        }

        //set text
        tvName.setText(name);
        tvDesc.setText(description);
        tvPrice.setText(price);
        tvPhone.setText(phone);

        //intent to launch phone call
        tvPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callContact();
            }
        });
    }

    private void callContact() {
        Intent intent = new Intent(Intent.ACTION_DIAL); //explicit
        intent.setData( Uri.parse("tel:" + phone)); //here we set the phone number to be called
        startActivity(intent);
    }
}
