package com.emobilis.firstandroidclass;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.emobilis.firstandroidclass.adapter.RecyclerAdapter;
import com.emobilis.firstandroidclass.model.RecyclerModel;

import java.util.ArrayList;

public class RecyclerViewActivity extends AppCompatActivity {
    //declare the element to display text
    TextView textName;
    //create a variable that will store the information i am receiving
    String nameOfUser;
    //referencing recyclerview
    private RecyclerView mRecyclerView;
    //referencing adapter
    private RecyclerView.Adapter mAdapter;
    //layout manager
    private RecyclerView.LayoutManager mLayoutManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler_view);
        //reference to the display
        textName = findViewById(R.id.textName);
        //fetch my shared info
        Intent intentRecycler = getIntent();
        nameOfUser = intentRecycler.getStringExtra("nameofuser");
        //toast
        Toast.makeText(this, "welcome " + nameOfUser, Toast.LENGTH_SHORT).show();
        //set the user input to the textview for display
        textName.setText("Welcome to the recyclerview class " + nameOfUser);


        //here we set up our list
        ArrayList<RecyclerModel> exampleList = new ArrayList<>();
        exampleList.add(new RecyclerModel(R.drawable.ic_whatshot_black_24dp, "Milk", "Glutton free milk"));
        exampleList.add(new RecyclerModel(R.drawable.ic_crop_square_black_24dp, "Sugar", "1 kilogram at 1000"));
        exampleList.add(new RecyclerModel(R.drawable.ic_all_inclusive_black_24dp, "Hisense", "Android Smart TV"));

        //create and populate the recyclerview
        mRecyclerView = findViewById(R.id.recyclerProducts);
        mRecyclerView.setHasFixedSize(true); //use size of arraylist items
        mLayoutManager = new LinearLayoutManager(this);
        mAdapter = new RecyclerAdapter(exampleList);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);



    }
}
