package com.emobilis.firstandroidclass;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.RelativeLayout;

public class SplashScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final int SPLASH_TIME_OUT = 3000; //the timing here is in seconds //3 seconds
        setContentView(R.layout.activity_splash_screen);
        //now we create the handler : what will count the time in my device
        //post delayed checks for a timing before executing a function
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //dictate where to move next
                Intent intent = new Intent(SplashScreen.this, ProductRecyclerView.class);
                startActivity(intent);
            }
        },SPLASH_TIME_OUT);
    }
}
