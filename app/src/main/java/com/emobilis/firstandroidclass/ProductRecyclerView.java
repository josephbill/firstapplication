package com.emobilis.firstandroidclass;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.emobilis.firstandroidclass.adapter.ProductAdapter;
import com.emobilis.firstandroidclass.adapter.RecyclerAdapter;
import com.emobilis.firstandroidclass.model.ProductModel;

import java.util.ArrayList;

public class ProductRecyclerView extends AppCompatActivity {
    //declare our recyclerview widget
    RecyclerView recyclerView;
    //declare our Adapter
    ProductAdapter productAdapter;
    //layout manager
    private  RecyclerView.LayoutManager layoutManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_recycler_view);

        //reference our recyclerview

        //set up list
        ArrayList<ProductModel> productModelArrayList = new ArrayList<>();
        //add to the arraylist"
        productModelArrayList.add(new ProductModel("Smartphone","Samsung A5 2020","KES 20,000","0798501657",R.drawable.ic_smartphone_black_24dp));
        productModelArrayList.add(new ProductModel("Bicycle","BMX Mountain Bike","KES 8,000","0798501657",R.drawable.ic_directions_bike_black_24dp));
        productModelArrayList.add(new ProductModel("Toyota Mazda","Mazda version 2020","KES 2,000,000","0798501657",R.drawable.ic_directions_car_black_24dp));
        productModelArrayList.add(new ProductModel("Coding in JavaScript","Beginner level documentation","KES 500","0798501657",R.drawable.ic_library_books_black_24dp));
        //populate the recycleviewer
        recyclerView = findViewById(R.id.recyclerNewProducts);
        recyclerView.setHasFixedSize(true); //use size of arraylist items
        layoutManager = new LinearLayoutManager(this); //intializing the layout manager
        productAdapter = new ProductAdapter(this,productModelArrayList); //new instance
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(productAdapter);
    }
}
