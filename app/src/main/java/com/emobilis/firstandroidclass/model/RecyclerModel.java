package com.emobilis.firstandroidclass.model;

public class RecyclerModel {
    //set up our object blueprint
    private int imageResource;
    private String productName;
    private String productDescription;

    public RecyclerModel(int image,String name, String Description){
        productName = name;
        productDescription = Description;
        imageResource = image;
    }

    //get and set


    public int getImageResource() {
        return imageResource;
    }

    public void setImageResource(int imageResource) {
        this.imageResource = imageResource;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }
}
