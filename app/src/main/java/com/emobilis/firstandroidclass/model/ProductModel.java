package com.emobilis.firstandroidclass.model;

public class ProductModel {
    //declare our datatypes / declare variables
    private String productName;
    private String productDescription;
    private String productPrice;
    private String phoneContact;
    private int imageResource;

    //create an empty constructor

    //create a constructor
    public ProductModel(String product_name,String product_desc,String product_price,String phone_contact,int image_resource){
        productName = product_name;
        productDescription = product_desc;
        productPrice = product_price;
        phoneContact = phone_contact;
        imageResource = image_resource;
    }

    //getters and setters

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public String getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(String productPrice) {
        this.productPrice = productPrice;
    }

    public String getPhoneContact() {
        return phoneContact;
    }

    public void setPhoneContact(String phoneContact) {
        this.phoneContact = phoneContact;
    }

    public int getImageResource() {
        return imageResource;
    }

    public void setImageResource(int imageResource) {
        this.imageResource = imageResource;
    }
}
