package com.emobilis.firstandroidclass.adapter;

import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.emobilis.firstandroidclass.R;
import com.emobilis.firstandroidclass.model.RecyclerModel;

import java.util.ArrayList;

public  class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.RecyclerViewHolder> {
    private ArrayList<RecyclerModel> recyclerModels;

    //empty constructor
    public RecyclerAdapter(ArrayList<RecyclerModel> exampleList) {
        recyclerModels = exampleList;
    }

    //create my viewholder subclass
    public static class RecyclerViewHolder extends RecyclerView.ViewHolder{
        //declare the elements
        TextView name,description;
        ImageView imageProduct;
        public RecyclerViewHolder(@NonNull View itemView) {
            super(itemView);
            //fetch / find / reference
            name = itemView.findViewById(R.id.textView);
            description = itemView.findViewById(R.id.textView2);
            imageProduct = itemView.findViewById(R.id.imageView);
        }
    }


    @NonNull
    @Override
    //we will inflate our recycled item layout class
    public RecyclerAdapter.RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycled_item, parent, false);
        RecyclerViewHolder evh = new RecyclerViewHolder(v); //new instance
        return evh;
    }

    @Override
    //here we will set data and if any interaction is needed this is the method that handle such
    public void onBindViewHolder(@NonNull RecyclerAdapter.RecyclerViewHolder holder, int position) {
        //give content to our recycled views
        RecyclerModel recyclerModelList = recyclerModels.get(position);

        //set data
        holder.name.setText(recyclerModelList.getProductName()); //name
        holder.description.setText(recyclerModelList.getProductDescription()); //product
        holder.imageProduct.setImageResource(recyclerModelList.getImageResource()); //image

    }

    @Override
    //this will recycler my item according to list
    public int getItemCount() {
        return recyclerModels.size();
    }
}
