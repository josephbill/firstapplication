package com.emobilis.firstandroidclass.adapter;


import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.emobilis.firstandroidclass.ProductDetails;
import com.emobilis.firstandroidclass.R;
import com.emobilis.firstandroidclass.model.ProductModel;
import com.emobilis.firstandroidclass.model.RecyclerModel;

import java.util.ArrayList;

//first thing is to inherit methods from the RecyclerView
public class ProductAdapter  extends RecyclerView.Adapter<ProductAdapter.RecyclerViewHolder> {
    private ArrayList<ProductModel> recyclerModelsList;
    Context mCtx;

    //constructor
    public ProductAdapter(Context context,ArrayList<ProductModel> productModelArrayList) {
        mCtx = context;
        recyclerModelsList = productModelArrayList;
    }

    //create my viewholder subclass
    public static class RecyclerViewHolder extends RecyclerView.ViewHolder{
        //declare the elements
        TextView name,description;
        ImageView imageProduct;
        RelativeLayout relaClick;
        public RecyclerViewHolder(@NonNull View itemView) {
            super(itemView);
            //fetch / find / reference
            name = itemView.findViewById(R.id.productName);
            description = itemView.findViewById(R.id.productDesc);
            imageProduct = itemView.findViewById(R.id.imageProduct);
            relaClick = itemView.findViewById(R.id.relaClick);
        }
    }


    @NonNull
    @Override
    //we will inflate our recycled item layout class
    public ProductAdapter.RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_display, parent, false);
        ProductAdapter.RecyclerViewHolder evh = new ProductAdapter.RecyclerViewHolder(v); //new instance
        return evh;
    }

    @Override
    //here we will set data and if any interaction is needed this is the method that handle such
    public void onBindViewHolder(@NonNull ProductAdapter.RecyclerViewHolder holder, final int position) {
        //give content to our recycled views
        final ProductModel recyclerModelList = recyclerModelsList.get(position);

        //set data
        holder.name.setText(recyclerModelList.getProductName()); //name
        holder.description.setText(recyclerModelList.getProductDescription()); //product
        holder.imageProduct.setImageResource(recyclerModelList.getImageResource()); //image
        holder.relaClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent  = new Intent(mCtx, ProductDetails.class);
                intent.putExtra("productName",recyclerModelsList.get(position).getProductName()); //name
                intent.putExtra("productDesc",recyclerModelsList.get(position).getProductDescription()); //desc
                intent.putExtra("productPrice",recyclerModelsList.get(position).getProductPrice()); //price
                intent.putExtra("contactPhone",recyclerModelsList.get(position).getPhoneContact()); //phone
                intent.putExtra("productImage",recyclerModelsList.get(position).getImageResource()); //image
                mCtx.startActivity(intent);
            }
        });


    }

    @Override
    //this will recycler my item according to list
    public int getItemCount() {
        return recyclerModelsList.size();
    }
}
